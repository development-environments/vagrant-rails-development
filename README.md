# Vagrant Rails Development Environment

This is a Vagrant starter configuration for rails development.

No project level rails configuration is included. This is meant
to be forked for starting rails projects you'd like to use Vagrant
for managing the development environment.

Based on Ubuntu/Xenial (16.04)

## Dependencies

* You must have Vagrant installed
* By default, the Vagrant provider is VirtualBox

## Instructions:

1. Fork this repository
1. Clone the forked repository
1. cd into the locally cloned directory
1. Run `vagrant up` - _this will take a while_

At this point you should have a vagrant environment created, 
provisioned, and running. Normal `vagrant` commands should work.

You can now configure your rails project.
