#!/usr/bin/env bash

NODE_VERSION=6.11
RUBY_VERSION=2.4.1

echo "--- UPDATING OS DEPENDENCIES ---"

sudo apt-get update
sudo apt-get upgrade
sudo apt-get -y -q=2 install build-essential
sudo apt-get -y -q=2 install gnupg2
sudo apt-get -y -q=2 install git
sudo apt-get -y -q=2 install libcurl3 libcurl3-gnutls libcurl4-openssl-dev
sudo apt-get -y -q=2 install libxml2 libxml2-dev libxslt1-dev libpq-dev
sudo apt-get -y -q=2 install postgresql-common postgresql libpq-dev postgresql-contrib
sudo apt-get -y -q=2 install imagemagick

echo "--- INSTALLING NVM ---"
if [[ ! -x "$HOME/.nvm" ]]; then
  wget -qO- https://raw.githubusercontent.com/creationix/nvm/v0.33.2/install.sh | bash

  echo '# Node Version Manager'  >> ~/.profile
  echo 'export NVM_DIR="$HOME/.nvm"'  >> ~/.profile
  echo '[ -s "$NVM_DIR/nvm.sh" ] && \. "$NVM_DIR/nvm.sh"'  >> ~/.profile
  echo '[ -s "$NVM_DIR/bash_completion" ] && \. "$NVM_DIR/bash_completion"'  >> ~/.profile

  export NVM_DIR="$HOME/.nvm"
  [ -s "$NVM_DIR/nvm.sh" ] && \. "$NVM_DIR/nvm.sh"
  [ -s "$NVM_DIR/bash_completion" ] && \. "$NVM_DIR/bash_completion"
else
  echo "OK"
fi

echo "--- INSTALLING NODE.JS ---"
if ! node --version; then
  nvm install $NODE_VERSION
  nvm use $NODE_VERSION
else
  echo 'OK'
fi

echo "--- INSTALLING RVM ---"
gpg2 --recv-keys 409B6B1796C275462A1703113804BB82D39DC0E3
\curl -sSL https://get.rvm.io | bash -s stable --quiet-curl

echo "--- INSTALLING RUBY $RUBY_VERSION ---"
source $HOME/.rvm/scripts/rvm
rvm get head
rvm install ruby-$RUBY_VERSION
rvm use ruby-$RUBY_VERSION@global
gem update --system --no-ri --no-rdoc
gem update --no-ri --no-rdoc
gem install bundler rails rspec-rails cucumber-rails pg redis-rails webpacker --no-ri --no-rdoc
rvm use ruby-$RUBY_VERSION --default


echo "--- INSTALLING YARN ---"
\curl -o- -L https://yarnpkg.com/install.sh | bash

# Add bundle, rake, yarn, commands bellow


